public class SSHConnection : GLib.Object {
    private SSH.Session session;
    private SSH.Channel fw_channel;

    private void verify_knownhost() {
        SSH.KnownHostState state;
        uint8[] hash = null;
        SSH.Key? srv_pubkey = null;
        size_t hlen;
        char[10] buff;
        string hexa;
        string p;
        int cmp;
        int rc;

        rc = this.session.get_server_publickey(out srv_pubkey);
        if (rc < 0) {
            error("Could not get publickey");
        }

        SSH.get_publickey_hash(srv_pubkey, SSH.KeyHashType.SHA1, out hash);
        if (rc < 0) {
            error("Could not get pubkey hash");
        }

        state = this.session.session_is_known_server();
        switch (state) {
            case SSH.KnownHostState.OK:
                break;
            case SSH.KnownHostState.CHANGED:
                SSH.print_hash( SSH.KeyHashType.SHA1, hash);
                SSH.clean_pubkey_hash(ref hash);
                this.session.disconnect();
                error("Host key for server changed");
            case SSH.KnownHostState.OTHER:
                SSH.clean_pubkey_hash(ref hash);
                this.session.disconnect();
                message("The host key for this server was not "+
                      "found but an other type of key exists");
                error("An attacker might change the default server key to" +
                    "confuse your client into thinking the key does not exist");
            case SSH.KnownHostState.NOT_FOUND:
                message("Could not find known host file.");
                message("If you accept the host key here, the file will be" +
                    "automatically created.");
                message("NO PISS OFF! VERIFY THE SERVER WITH YOU SYSTEMS SSH CLIENT");
                break;
            case SSH.KnownHostState.UNKNOWN:
                hexa = SSH.get_hexa(hash);
                message("The server is unknown. Do you trust the host key?\n");
                message("Public key hash: %s\n", hexa);
                message("NO PISS OFF! VERIFY THE SERVER WITH YOU SYSTEMS SSH CLIENT");
                break;
            case SSH.KnownHostState.ERROR:
                SSH.clean_pubkey_hash(ref hash);
                this.session.disconnect();
                error("Known hosts error");
        }
        
    }

    private void  open_forward_tunnel() {
        this.fw_channel = this.session.channel_new();
        int rc;
        rc = this.fw_channel.open_forward("127.0.0.1", 1510, "localhost", 1337);
        if (rc != SSH.OK) {
            warning("Could not open forwarding channel");
            return;
        }
        message("tunnel offen");
    }

    public void disconnect() {
        if (this.fw_channel.is_open()>0) {
            this.fw_channel.close();
        }
        this.session.disconnect();
    }

    private void authenticate() {
        int rc;
        rc = this.session.userauth_publickey_auto(null, null);
        if (rc != SSH.Auth.SUCCESS ) {
                this.session.disconnect();
            error("Coudlnt authenticate");
        }
    }

    public SSHConnection(string hostname) {
        this.session =  SSH.Session.create();
        this.session.options_set(SSH.Options.HOST, hostname);
        this.session.connect();
        this.verify_knownhost();
        this.authenticate();
        this.open_forward_tunnel();
    }
}

public static int main() {
    var ssh_connection = new SSHConnection("napalmpint");
    var ml = new GLib.MainLoop();
    ml.run();
    return 0;
}

