/* libssh Vala Bindings
 * Copyright 2021 Daniel 'grindhold' Brendle <grindhold@gmx.net>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


[CCode (cheader_filename="libssh/libssh.h")]
namespace SSH {
    [CCode (cname="enum ssh_auth_e", cprefix = "SSH_AUTH_")]
    public enum Auth {
        SUCCESS=0,
        DENIED,
        PARTIAL,
        INFO,
        AGAIN,
        ERROR=-1
    }
    [CCode (cname="enum ssh_requets_e", cprefix = "SSH_REQUEST_")]
    public enum Requests {
        AUTH=1,
        CHANNEL_OPEN,
        CHANNEL,
        SERVICE,
        GLOBAL
    }

    [CCode (cname="enum ssh_channel_type_e", cprefix = "SSH_CHANNEL_")]
    public enum ChannelType {
        UNKNOWN=0,
        SESSION,
        DIRECT_TCPIP,
        FORWARDED_TCPIP,
        X11,
        AUTH_AGENT
    }
    
    [CCode (cname="enum ssh_channel_requests_e", cprefix = "SSH_CHANNEL_REQUEST_")]
    public enum ChannelRequests {
        UNKNOWN=0,
        PTY,
        EXEC,
        SHELL,
        ENV,
        SUBSYSTEM,
        WINDOW_CHANGE,
        X11
    }

    [CCode (cname="enum ssh_channel_requests_e", cprefix = "SSH_GLOBAL_REQUEST_")]
    public enum GlobalRequests {
        UNKNOWN=0,
        TCPIP_FORWARD,
        CANCEL_TCPIP_FORWARD,
        KEEPALIVE
    }

    [CCode (cname="enum ssh_publickey_state_e", cprefix = "SSH_PUBLICKEY_STATE_")]
    public enum PublicKeyState {
        ERROR=-1,
        NONE=0,
        VALID=1,
        WRONG=2
    }

    [CCode (cprefix="SSH_")]
    public const int CLOSED;    
    [CCode (cprefix="SSH_")]
    public const int READ_PENDING;
    [CCode (cprefix="SSH_")]
    public const int CLOSED_ERROR;
    [CCode (cprefix="SSH_")]
    public const int WRITE_PENDING;

    [CCode (cname="enum ssh_server_known_e", cprefix = "SSH_SERVER_")]
    public enum ServerKnown {
        ERROR=-1,
        NOT_KNOWN=0,
        KNOWN_OK,
        KNOWN_CHANGED,
        FOUND_OTHER,
        FILE_NOT_FOUND
    }

    [CCode (cname="enum ssh_known_hosts_e", cprefix = "SSH_KNOWN_HOSTS_")]
    public enum KnownHostState {
        /**
         * There had been an error checking the host.
         */
        ERROR = -2,

        /**
         * The known host file does not exist. The host is thus unknown. File will
         * be created if host key is accepted.
         */
        NOT_FOUND = -1,

        /**
         * The server is unknown. User should confirm the public key hash is
         * correct.
         */
        UNKNOWN = 0,

        /**
         * The server is known and has not changed.
         */
        OK,

        /**
         * The server key has changed. Either you are under attack or the
         * administrator changed the key. You HAVE to warn the user about a
         * possible attack.
         */
        CHANGED,

        /**
         * The server gave use a key of a type while we had an other type recorded.
         * It is a possible attack.
         */
        OTHER,
    }

    public const int MD5_DIGEST_LEN;

    
    [CCode (cname="enum ssh_error_types_e", cprefix = "SSH_")]
    public enum ErrorType {
        NO_ERROR=0,
        REQUEST_DENIED,
        FATAL,
        EINTR
    }

    [CCode (cname="enum ssh_keytypes_e", cprefix = "SSH_KEYTYPE_")]
    public enum KeyType {
        UNKNOWN=0,
        DSS=1,
        RSA,
        RSA1,
        ECDSA, /* deprecated */
        ED25519,
        DSS_CERT01,
        RSA_CERT01,
        ECDSA_P256,
        ECDSA_P384,
        ECDSA_P521,
        ECDSA_P256_CERT01,
        ECDSA_P384_CERT01,
        ECDSA_P521_CERT01,
        ED25519_CERT01
    }

    [CCode (cname="enum ssh_pulickkey_hash_type", cprefix = "SSH_PUBLICKEY_HASH_")]
    public enum KeyHashType {
        SHA1,
        MD5,
        SHA256
    }

    [CCode (cname="enum ssh_keycmp_e", cprefix = "SSH_KEY_CMP_")]
    public enum KeyCmp {
        PUBLIC=0,
        PRIVATE
    }

    [CCode (cname="ssh_knownhosts_entry", free_function="ssh_knownhost_entry_free")]
    public struct KnownHostsEntry {
        string hostname;
        string unparsed;
        Key publickey;
        string comment;
    }

    [CCode (cprefix="SSH_")]
    public const int ADDRSTRLEN;

    [CCode (prefix="SSH_")]
    public const int OK;
    [CCode (prefix="SSH_")]
    public const int ERROR;
    [CCode (prefix="SSH_")]
    public const int AGAIN;
    [CCode (prefix="SSH_")]
    public const int EOF;

    /* TODO: original file contains a anonymous enum with
     *       log options init. check if relevant and if so
     *       how to bind to it in this vapi
     */

    [CCode (prefix="SSH_")]
    public const int LOG_NONE;
    [CCode (prefix="SSH_")]
    public const int LOG_WARN;
    [CCode (prefix="SSH_")]
    public const int LOG_INFO;
    [CCode (prefix="SSH_")]
    public const int LOG_DEBUG;
    [CCode (prefix="SSH_")]
    public const int LOG_FUNCTIONS;

    [CCode (cname="enum ssh_options_e", cprefix = "SSH_OPTIONS_")]
    public enum Options {
        HOST,
        PORT,
        PORT_STR,
        FD,
        USER,
        SSH_DIR,
        IDENTITY,
        ADD_IDENTITY,
        KNOWNHOSTS,
        TIMEOUT,
        TIMEOUT_USEC,
        SSH1,
        SSH2,
        LOG_VERBOSITY,
        LOG_VERBOSITY_STR,
        CIPHERS_C_S,
        CIPHERS_S_C,
        COMPRESSION_C_S,
        COMPRESSION_S_C,
        PROXYCOMMAND,
        BINDADDR,
        STRICTHOSTKEYCHECK,
        COMPRESSION,
        COMPRESSION_LEVEL,
        KEY_EXCHANGE,
        HOSTKEYS,
        GSSAPI_SERVER_IDENTITY,
        GSSAPI_CLIENT_IDENTITY,
        GSSAPI_DELEGATE_CREDENTIALS,
        HMAC_C_S,
        HMAC_S_C,
        PASSWORD_AUTH,
        PUBKEY_AUTH,
        KBDINT_AUTH,
        GSSAPI_AUTH,
        GLOBAL_KNOWNHOSTS,
        NODELAY,
        PUBLICKEY_ACCEPTED_TYPES,
        PROCESS_CONFIG,
        REKEY_DATA,
        REKEY_TIME
    }

    /* TODO: original file contains a anonymous enum with
     *       write/read/recursive. check if relevant and if so
     *       how to bind to it in this vapi
     */

    [CCode (cname="enum ssh_scp_request_types_e", cprefix = "SSH_SCP_REQUEST_")]
    public enum ScpRequestType {
        /** A new directory is going to be pulled */
        NEWDIR=1,
        /** A new file is going to be pulled */
        NEWFILE,
        /** End of requests */
        EOF,
        /** End of directory */
        ENDDIR,
        /** Warning received */
        WARNING
    }

    [CCode (cname="enum ssh_connector_flags_e", cprefix = "SSH_CONNECTOR_")]
    public enum ConnectorFlag {
        /** Only the standard stream of the channel */
        STDOUT = 1,
        STDINOUT = 1,
        /** Only the exception stream of the channel */
        STDERR = 2,
        /** Merge both standard and exception streams */
        BOTH = 3
    }

    [CCode (cname="struct ssh_session_struct", free_function="ssh_free", cprefix="ssh_", has_type_id=false)]
    [Compact]
    public class Session{
        [CCode (cname="ssh_new")]
        public static Session? create();

        public int connect();
        public int disconnect();
        public Channel channel_new();
        public int blocking_flush(int timeout);
        public Channel? channel_accept_forward(int timeout_ms, ref int destination_port);
        public int channel_cancel_forward(string address, int port);
        public int channel_listen_forward(string address, int port, ref int bound_port);
        public unowned string get_disconnect_message();
        public int get_fd();
        public string get_issue_banner();
        public int get_openssh_version();
        public int get_server_publickey(out Key key);
        public int get_server_by_publickey(Key key);
        public int get_random(void* where, int len, int strong);
        public int get_version();
        public int get_status();
        public int get_poll_flags();
        public int is_blocking();
        public int is_connected();
        public KnownHostState session_hash_known_hosts_entry();
        public int session_export_known_hosts_entry(out string pentry_string);
        public int session_update_known_hosts();
        public KnownHostState session_get_known_hosts_entry(out KnownHostsEntry khe);
        public KnownHostState session_is_known_server();
        public void _log(int priority, string format, ...);
        public Message message_get();
        public int options_copy(ref Session dest);
        public int options_getopt(out int argc, out string argv);
        public int options_parse_config(string filename);
        public int options_set(Options option, void* value);
        public int options_get(Options option, out void* value);
        public int options_get_port(out int port_target);

        public int send_ignore(string data);
        public int send_debug(string message, int always_display);
        public int gssapi_set_creds(void* creds);
        public Scp scp_new(int mode, string location);
        public int service_request(string service);
        public int set_agent_channel(Channel channel);
        public int set_agent_socket(int fd);
        public void set_blocking(int blocking);
        public void set_counters(Counter scounter, Counter rcounter);
        public void set_fd_except();
        public void set_fd_toread();
        public void set_fd_towrite();
        public void silent_disconnect();
        public int set_pcap_file(Pcapfile pcapfile);
        public int userauth_none(string username);
        public int userauth_list(string username);
        public int userauth_try_publickey(string username, Key publickey);
        public int userauth_publickey(string username, Key publickey);
        public int userauth_agent(string username);
        public int userauth_publickey_auto(string username, string? passphrase);
        public int userauth_password(string username, string password);
        public int userauth_kbdint(string username, string submethods);
        public string userauth_kbdint_getinstruction();
        public string userauth_kbdint_getname();
        public int userauth_kbdint_getnprompts();
        public string userauth_kbdint_getprompt(uint i, string echo);
        public int userauth_kbdint_getnanwsers();
        public string userauth_kbdint_getanswer(uint i);
        public int userauth_kbdint_setanswer(uint i, string answer);
        public int userauth_gssapi();

        public string get_clientbanner();
        public string get_serverbanner();
        public string get_kex_algo();
        public string get_cipher_in();
        public string get_chipher_out();
        public string get_hmac_in();
        public string get_hmac_out();
    }

    [CCode (cname="ssh_init")]
    public int init();

    [CCode (cname="struct ssh_channel_struct", free_function="ssh_channel_free", cprefix="ssh_channel_")]
    [Compact]
    public class Channel {
        [CCode (cname="ssh_channel_new")]
        public Channel(Session s);

        public Channel accept_x11(int timeout_ms);
        public int change_pty_size(int cols, int rows);
        public int close();
        public int get_exit_status();
        public Session get_session();
        public int is_closed();
        public int is_eof();
        public int is_open();
        public int open_auth_agent();
        public int open_forward(string remotehost, int remoteport, string sourcehost, int localport);
        public int open_forward_unix(string remotepath, string sourcehost, int localport);
        public int open_session();
        public int open_x11(string orig_addr, int orig_port);
        public int poll(int is_stderr);
        public int poll_timeout(int timeout, int is_stderr);
        public int read(void* dest, uint32 count, int is_stderr);
        public int read_timeout(void* dest, uint32 count, int is_stderr, int timeout_ms);
        public int read_nonblocking(void* dest, uint32 count, int is_stderr);
        public int request_env(string name, string value);
        public int request_exec(string cmd);
        public int request_pty();
        public int request_pty_size(string term, int cols, int rows);
        public int request_shell();
        public int request_send_signal(string signum);
        public int request_send_break(uint32 length);
        public int request_sftp();
        public int request_subsystem(string subsystem);
        public int request_x11(int single_connection, string protocol, string cookie, int screennum);
        public int request_auth_agent();
        public int send_eof();
        public static int select(ref Channel readchans, ref Channel writechans, ref Channel exceptchans, Posix.timeval timeout);
        public void set_blocking(int blocking);
        public void set_counter(Counter counter);
        public int write(void* data, uint32 len);
        public int write_stderr(void* data, uint32 len);
        public uint32 window_size();
    }

    [CCode (cname="struct ssh_connector_struct", free_function="ssh_connector_free", cprefix="ssh_connector_")]
    [Compact]
    public class Connector {
        [CCode (cname="ssh_connector_new")]
        public Connector(Session s);
        public int set_in_channel(Channel channel, ConnectorFlag flags);
        public int set_out_channel(Channel channel, ConnectorFlag flags);
        public void set_in_fd(int fd);
        public void set_out_fd(int fd);
    }

    [CCode (cname="struct ssh_key_struct", free_function="ssh_key_free", cprefix="ssh_key_")]
    [Compact]
    public class Key {
        [CCode (cname="ssh_key_new")]
        public Key();

        public KeyType type();
        public static string type_to_char(KeyType key);
        public static KeyType type_from_name(string typename);
        public int is_public();
        public int is_private();
        public static int cmp(Key k1, Key k2, KeyCmp what);
    }

    [CCode (cname="ssh_auth_callback", has_target=false)]
    public delegate int AuthCallback(string prompt, char[] buf, size_t len, int echo, int verify, void* userdata); 

    [CCode (cprefix="ssh_pki")]
    namespace Pki {
        public int generate(KeyType type, int parameter, out Key pkey);
        public int import_privkey_base64(string b64_key, string passphrase, AuthCallback cb, void* auth_data, out Key pkey);
        public int export_privkey_base64(Key privkey, string passphrase, AuthCallback cb, void* auth_data, out string b64_key);
        public int import_privkey_file(string filename, string passphrase, AuthCallback cb, void* auth_data, out Key pkey);
        public int export_privkey_file(Key privkey, string passphrase, AuthCallback cb, void* auth_data, string filename);
        public int copy_cert_to_privkey(Key cert_key, Key privkey);
        public int import_pubkey_base64(string b64_key, KeyType type, out Key pkey);
        public int import_pubkey_file(string filename, out Key pkey);
        public int import_cert_base64(string b64cert, KeyType type, out Key pkey);
        public int import_cert_file(string filename, out Key pkey);
        public int export_privkey_to_pubkey(Key privkey, out Key pkey);
        public int export_pubkey_base64(Key key, out string b64_key);
        public int export_pubkey_file(Key key, string filename);
        public unowned string key_ecdsa_name(Key key);
    }

    [CCode (cname="ssh_get_fingerprint_hash")]
    public string get_fingerprint_hash(KeyType type, [CCode (array_length_type="size_t")]uint8[] hash);

    [CCode (cname="ssh_print_hash")]
    public void print_hash(KeyHashType type, [CCode (array_length_type="size_t")]uint8[] hash);

    [CCode (cname="ssh_print_hexa")]
    public void print_hexa(string descr, uint8* what, size_t len);

    [CCode (cname="ssh_get_publickey_hash")]
    public int get_publickey_hash(Key key, KeyHashType type, [CCode (array_length_type="size_t")]out uint8[] hash);

    [CCode (cname="ssh_clean_pubkey_hash")]
    public void clean_pubkey_hash([CCode (array_length=false)]ref uint8[] hash);


    [CCode (cname="ssh_get_hexa")]
    public string get_hexa(uint8[] hash);

    [CCode (cname="struct ssh_message_struct", free_function="ssh_message_free", cprefix="ssh_message_")]
    [Compact]
    public class Message {
        public Channel channel_request_open_reply_accept();
        public int channel_request_open_reply_accept_channel(Channel channel);
        public int channel_request_reply_success();
        public int subtype();
        public int type();
    }

    [CCode (cname="struct ssh_scp_struct", free_function="ssh_scp_free", cprefix="ssh_scp_")]
    [Compact]
    public class Scp {
        [CCode (cname="ssh_scp_new")]
        public Scp(Session session, int mode, string location);

        public int accept_reqeust();
        public int close();
        public int deny_request(string reason);
        public int init();
        public int leave_directory();
        public int pull_request();
        public int push_directory(string dirname, int mode);
        public int push_file(string filename, size_t size, int perms);
        public int push_file64(string filename, size_t size, int perms);
        public int read(void* buffer, size_t size);
        public unowned string request_get_filename();
        public int request_get_permissions();
        public size_t request_get_size();
        public uint64 request_get_size64();
        public unowned string request_get_warning();
        public int write(void* buffer, size_t len);
    }

    [CCode (cname="ssh_event_callback", has_target=false)]
    public delegate int EventCallback(int fd, int revents, void* userdata);

    [CCode (cname="struct ssh_event_struct", free_function="ssh_event_free", cprefix="ssh_event_")]
    public class Event {
        [CCode (cname="ssh_event_new")]
        public Event();
        public int add_fd(int fd, short events, EventCallback cb, void* userdata);
        public int add_session(Session session);
        public int add_connector(Connector connector);
        public int dopoll(int timeout);
        public int remove_fd(int fd);
        public int remove_session(Session session);
        public int remove_connector(Connector connector);
    }

    [CCode (cname="struct ssh_counter_struct", free_function="free", cprefix="ssh_")]
    [Compact]
    public class Counter {
    }

    [CCode (cname="struct ssh_pcap_file_struct", free_function="ssh_pcap_file_free", cprefix="ssh_pcap_file_")]
    [Compact]
    public class Pcapfile {
        [CCode (cname="ssh_pcap_file_new")]
        public Pcapfile();
        public int close();
        public int open(string filename);
    }
}
